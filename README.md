﻿#MyWishList.app
*Projet développé par Quentin RIMET, Gautier STUHLFAUTH, Jérémy BRODIER, Quentin BARBIER et Kylian CONRAUX*

Principes généraux
------------------
MyWishList.app est une application en ligne pour créer, partager et gérer des listes de cadeaux.
L'application permet à un utilisateur de créer une liste de souhaits à l'occasion d'un événement particulier (anniversaire, fin d'année, mariage, retraite …) et lui permet de diffuser cette liste de souhaits à un ensemble de personnes concernées. Ces personnes peuvent alors consulter cette liste et s'engager à offrir 1 élément de la liste. Cet élément est alors marqué comme réservé dans cette liste.
On a donc 2 types d'utilisateurs de l'application :
 - les utilisateurs créateurs d'une liste de souhaits, qu'ils diffusent 
   à un ensemble de personnes
 - les utilisateurs qui consultent une liste de souhaits et
   éventuellement choisissent un (ou plusieurs) item(s) dans la liste.
   
Comment installer le projet :
------------------
Pour installer ce projet sur votre machine il vous faut juste la base de données (mySQL) suivante ainsi que la création d’un dossier indiqué après.

TABLE USER :
CREATE TABLE IF NOT EXISTS 'user' (
  'user_id' int(11) NOT NULL AUTO_INCREMENT,
  'username' varchar(500) NOT NULL,
  'password' varchar(500) NOT NULL,
  PRIMARY KEY ('user_id')
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


TABLE LISTE :
CREATE TABLE IF NOT EXISTS 'liste' (
  'no' int(11) NOT NULL AUTO_INCREMENT,
  'user_id' int(11) NOT NULL,
  'titre' varchar(255) NOT NULL,
  'description' text NOT NULL,
  'expiration' date NOT NULL,
  'token' varchar(255) NOT NULL,
  'libre' varchar(45) NOT NULL,
  PRIMARY KEY ('no')
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

TABLE ITEM:
CREATE TABLE IF NOT EXISTS 'item' (
  'id' int(11) NOT NULL AUTO_INCREMENT,
  'liste_id' int(11) NOT NULL,
  'nom' text NOT NULL,
  'descr' text NOT NULL,
  'img' text NOT NULL,
  'url' text NOT NULL,
  'tarif' decimal(5,2) NOT NULL,
  'username' varchar(200) NOT NULL,
  'message' text NOT NULL,
  PRIMARY KEY ('id')
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

Dans la racine du dossier créer le dossier /conf/
A l’intérieur de celui-ci créer le fichier « conf.ini » est remplissez le de la manière suivante :
username= « votre login de votre base de donnée »
password= « votre mot de passe de votre base de donnée »
driver=mysql
host=localhost (ou le nom de votre serveur si celui-ci est distant)
database= « le nom de votre base de donnée »
charset=utf8
collation=utf8_unicode_ci


Donner permettant de tester le projet :
------------------
Insertion d’utilisateur : On peut se connecter sur le compte « diablox9 » à l’aide du mot de passe « callof »

INSERT INTO 'user' ('user_id', 'username', 'password') VALUES
(NULL, 'gaustu', '$2y$12$LKxRHTGuug7ZKdcNXJ9nBe1S41A5aoDCchCZ7ekRKfqXXbo9M1E4S'),
(NULL, 'svz', '$2y$12$2FaRcRWuud.ohZdKe.7eU.GoUA.4NQq.9z4E5/z9VmQbmcqww3dva'),
(NULL, 'diablox9', '$2y$12$nicuhXpie1yLDFpDRXwlReovk1x0tlH/UWQqJu4wDtTWJToDB0C9K');

Insertion de liste :
INSERT INTO 'liste' ('no', 'user_id', 'titre', 'description', 'expiration', 'token', 'libre') VALUES
(NULL, 3, 'Annniversaire', 'Chut, il faut pas le dire', '2018-01-19', '139215', 'oui'),
(NULL, 3, 'Liste secret', 'Ah aha aha ah(rire démoniaque)', '2018-04-20', '44319', 'oui'),
(NULL, 4, 'Trickshot', 'sur rust', '2018-01-31', '40298', 'oui'),
(NULL, 4, 'Voiture', 'fait de A à Z', '2018-01-16', '125483', 'non');

Insertion d’item :
INSERT INTO 'item' ('id', 'liste_id', 'nom', 'descr', 'img', 'url', 'tarif', 'username', 'message') VALUES
(NULL, 2, 'Livre', 'Une livre', '', '', '15.00', '', ''),
(NULL, 3, 'Invoquer satan ', 'conquerire le monde', 'http://www.godandscience.org/images/devil.jpg', 'http://www.godandscience.org/images/devil.jpg', '666.00', '', ''),
(NULL, 5, 'Moteur', 'un moteur', '', '', '200.00', 'gaustu', 'péte pas la batterie'),
(NULL, 3, 'Acheter le cerber', 'Parcequ&#39;il est trop mignon', 'https://static.wamiz.fr/images/comics/stuffed/large/cerber-90691.jpg', 'https://static.wamiz.fr/images/comics/stuffed/large/cerber-90691.jpg', '666.00', '', ''),
(NULL, 5, 'Carbu', 'c le carbu', '', '', '100.00', '', ''),
(NULL, 3, 'os', 'pour le cerbert', 'https://comps.canstockphoto.fr/os-garder-tas-chien-clipart_csp10200912.jpg', 'https://comps.canstockphoto.fr/os-garder-tas-chien-clipart_csp10200912.jpg', '1.00', '', ''),
(NULL, 3, 'mougeon', 'Les meilleur espion', 'https://www.agoravox.fr/local/cache-vignettes/L150xH150/auton19690-f889f.png', 'https://www.agoravox.fr/local/cache-vignettes/L150xH150/auton19690-f889f.png', '120.00', '', ''),
(NULL, 4, 'awp', 'sniper', 'https://vignette.wikia.nocookie.net/cswikia/images/6/69/P_awp.png/revision/latest?cb=20151111053214', '', '50.00', 'MrLeV12', 'Pour toi Benoit &#60;3'),
(NULL, 4, 'tomahawk', 'pour titanic noscope', 'https://i.ytimg.com/vi/YAZlNDdcvAM/maxresdefault.jpg', '', '0.00', '', ''),
(NULL, 4, 'killcam', 'pour youtube', '', 'https://www.youtube.com/watch?v=mLRbZJS5A_E', '50.00', '', '');
