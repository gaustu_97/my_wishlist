<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 18/12/2017
 * Time: 11:37
 */

namespace mywishlist\models;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "user";
    protected $primaryKey = "user_id";

    public $timestamps=false;

}