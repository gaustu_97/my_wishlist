<?php

require_once __DIR__.'/vendor/autoload.php';
use mywishlist\models\Item as Item ;
use mywishlist\models\Liste as Liste;
use mywishlist\controleur\ControleurItem;
use mywishlist\controleur\ControleurListe;

use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$config = parse_ini_file("conf/conf.ini");
$db->addConnection($config);
$db->setAsGlobal();
$db->bootEloquent();
session_start();

$app = new \Slim\Slim();

$app->get("/", function () {
    $accueil="<h1>Bienvenue sur MyWishList</h1><h3>L'application qui vous permettra de gérer vos listes de cadeaux facilement</h3></br><h3>Vous êtes nouveaux ?</h3> <p>Enregistrez vous puis connectez vous</p>";
    echo (new mywishlist\vue\VuePrincipale())->render($accueil);
})->name('acceuil');

$app->get("/index.html", function () {
    $accueil="<h1>Bienvenue sur MyWishList</h1>";
    echo (new mywishlist\vue\VuePrincipale())->render($accueil);
});

$app->get("/listes/:no/items", function ($no) {
    $controller = new \mywishlist\controleur\ControleurListe();
    $controller->liste_item($no);
})->name('listes/items');



$app->get("/user/login", function () {
    $v=new \mywishlist\vue\VueConnection();
    echo $v->render(1);
})->name('connection');

$app->post("/user/login", function () {
    $app= \Slim\Slim::getInstance();
    if(isset($_POST['valider_inc']) && $_POST['valider_inc']="validate_f1"){
        if(\mywishlist\controleur\Authentification::authentificate(filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS),filter_var($_POST['pwd'], FILTER_SANITIZE_SPECIAL_CHARS)))
            $app->redirect($app->urlFor('acceuil'));
            else $app->redirect($app->urlFor('connection'));
    }

});

$app->get("/user/create", function () {
    $v=new \mywishlist\vue\VueConnection();
    echo $v->render(2);
})->name('enregistrer');

$app->post("/user/create", function () {
    $app= \Slim\Slim::getInstance();
    if(isset($_POST['valider_inc']) && $_POST['valider_inc']="validate_f1"&& isset($_POST['name'])){
        \mywishlist\controleur\Authentification::createUser(filter_var($_POST['name'], FILTER_SANITIZE_SPECIAL_CHARS),filter_var($_POST['pwd'], FILTER_SANITIZE_SPECIAL_CHARS));
    }
    $app->redirect($app->urlFor('acceuil'));
})->name("creation");

$app->get("/listes", function () {
    $listes = new ControleurListe();
    $listes->liste();
})->name("listes");


$app->post("/listes/:no", function ($id) {
    $app = \Slim\Slim::getInstance();
    $listes = new \mywishlist\controleur\ControleurListe();
    $listes->supprimer_liste($id);
    $app->redirect($app->urlFor('listes'));
})->name("suppressionListe");

$app->get("/liste/create", function () {
    $listes = new \mywishlist\controleur\ControleurListe();
	$listes->afficher_nListe();
})->name('creerListe');

$app->post("/liste/create", function () {
    $app = \Slim\Slim::getInstance();

    if(isset($_POST['valider_liste'])&&$_POST['valider_liste']="validate_f1"){
		$listes = new \mywishlist\controleur\ControleurListe();
		$listes->ajout_liste(filter_var($_POST['titre'] ,FILTER_SANITIZE_STRING),filter_var($_POST['descrip'] ,FILTER_SANITIZE_STRING),$_POST['expi']);
        $app->redirect($app->urlFor('listes'));
    }
	
})->name("creationListe");

$app->get("/listes/:no/createItem", function ($no) {
    $controller = new ControleurItem();
    $controller->afficher_ajout_item($no);
})->name('creerItem');

$app->post("/listes/:no/createItem", function ($no) {
    $app = \Slim\Slim::getInstance();

    if($app->request->post('valider_item')=="valid_item_function"){
        $controller = new ControleurItem();
		$nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
		$description = filter_var($_POST['descrip'], FILTER_SANITIZE_SPECIAL_CHARS);
		$prix = filter_var($_POST['prix'], FILTER_SANITIZE_SPECIAL_CHARS);
        $image=filter_var($_POST['image'], FILTER_SANITIZE_SPECIAL_CHARS);
        $url=filter_var($_POST['url'], FILTER_SANITIZE_SPECIAL_CHARS);
        $controller->ajout_item($nom, $description, $prix, $image,$url, $no);
    }

    $app->redirect($app->urlFor('listes/items', array('no'=>$no)));
});

$app->post("/modifl/:no", function ($no) {
    $app = \Slim\Slim::getInstance();

    if($app->request->post('valider_liste')=="valid_liste_function"){
        $controller = new ControleurListe();
        $titre = filter_var($_POST['titre'], FILTER_SANITIZE_SPECIAL_CHARS);
        $description = filter_var($_POST['descrip'], FILTER_SANITIZE_SPECIAL_CHARS);
        $exp = filter_var($_POST['exp'], FILTER_SANITIZE_SPECIAL_CHARS);

        $controller->modif_liste($titre, $description, $exp,$_POST['libre'],$no);
    }

    $app->redirect($app->urlFor('listes/items', array('no'=>$no)));
});

$app->get("/modif/:no/:id", function ($no,$id) {
    $controller = new ControleurItem();
    $controller->afficher_modif_item($no,$id);
})->name('modifitem');

$app->get("/modifl/:no", function ($no) {
    $controller = new ControleurListe();
    $controller->afficher_modif_liste($no);
})->name('modifliste');


$app->post("/modif/:no/:id", function ($no,$id) {
    $app = \Slim\Slim::getInstance();

    if($app->request->post('valider_item')=="valid_item_function"){
        $controller = new ControleurItem();
        $nom = filter_var($_POST['nom'], FILTER_SANITIZE_SPECIAL_CHARS);
        $description = filter_var($_POST['descrip'], FILTER_SANITIZE_SPECIAL_CHARS);
        $prix = filter_var($_POST['prix'], FILTER_SANITIZE_SPECIAL_CHARS);
        $image = filter_var($_POST['image'], FILTER_SANITIZE_SPECIAL_CHARS);
        $url=filter_var($_POST['url'], FILTER_SANITIZE_SPECIAL_CHARS);
        $controller->modif_item($nom, $description, $prix, $image,$url,$no,$id);
    }

    $app->redirect($app->urlFor('item', array('no'=>$no,'id'=>$id)));
});

$app->post("/items/supp/:id", function ($id) {
    if(isset($_POST['supprimer_item'])) {
        $app = \Slim\Slim::getInstance();
        $item = new \mywishlist\controleur\ControleurItem();
        $item->supprimer_item($id);
    }

    $app->redirect($app->urlFor('listes/items',array('no'=>$_POST['no'])));
})->name("suppressionItem");

$app->get('/profil', function () {
    $controller = new \mywishlist\controleur\Authentification();
    $controller->showProfileInformations();
})->name('profil');

$app->post("/profil", function () {
    $app = \Slim\Slim::getInstance();

    if (isset($_POST['maj_profil']) && $_POST['maj_profil'] == 'maj_profil') {
        $user = new \mywishlist\controleur\Authentification();
        $user->changeName(filter_var($_POST['name'], FILTER_SANITIZE_STRING));
    }
    $app->redirect('profil');
});

$app->get("/items/:no/:id", function ($no,$id) {
    $liste_id = new \mywishlist\controleur\ControleurItem();
    $liste_id->item($no,$id);
})->name('item');

$app->post("/items/:no/:id", function ($no,$id) {
    $app = \Slim\Slim::getInstance();


    if(isset($_POST['reserve_username'])) {
        $controller = new mywishlist\controleur\ControleurItem();
        $controller->reserver($no,$id, $_POST['reserve_username'],$_POST['reserve_message']);
    }

    $app->redirect($app->urlFor('item',array('no'=>$no,'id'=>"$id")));
});

$app->get("/libres",function (){
   $c=new ControleurListe();
   $c->libre();

})->name('libre');

$app->get("/profils/:id",function($id){
   $c=new \mywishlist\controleur\ControleurUser();
   $c->profils($id);
})->name('profils');

$app->post("/profils/:id",function($id){
    $app = \Slim\Slim::getInstance();


    if(isset($_POST['supp_user'])) {
        $c = new \mywishlist\controleur\ControleurUser();
        $c->supprimer_user($id);
    }

    $app->redirect($app->urlFor('acceuil'));
});

$app->get("/modifmdp/:id",function($id){
   $c=new \mywishlist\controleur\ControleurUser();
   $c->afficher_modif_profils($id);
})->name('modifmdp');

$app->post("/modifmdp/:id",function($id){

    $app = \Slim\Slim::getInstance();
    if(isset($_POST['valider_mdp'])) {
        $controller = new mywishlist\controleur\ControleurUser();
        $controller->modifiermdp($id,$_POST['mdp']);
    }
    $app->redirect($app->urlFor('profils',array('id'=>$id)));
});

$app->run();
