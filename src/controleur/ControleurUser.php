<?php

namespace mywishlist\controleur;

use Illuminate\Support\Facades\Session;
use mywishlist\models\Item as Item ;
use mywishlist\models\Liste as Liste;
use mywishlist\models\User;


class ControleurUser
{

public function supprimer_user($id)
{
    $l = User::where('user_id', '=', $id)->first();
    if (isset($_SESSION['user_id'])) {
    if ($l != null) {
    if ($_SESSION['user_id']  == $l->user_id) {
        $liste=Liste::where('user_id','=',$id)->get();
        foreach ($liste as $li) {
            $li->items()->delete();
            $li->delete();
        }
        $l->delete();
        session_destroy();
    } else {
        return false;
    }
        } else {
        return false;
    }
    }
}

public function profils($id){
$l=User::all()->where('user_id','=',$id)->first();
$v = new \mywishlist\vue\VueParticipant($l);
echo $v->render(10);
}

public function afficher_modif_profils($id){
    $l=User::all()->where('user_id','=',$id)->first();
    $v = new \mywishlist\vue\VueParticipant($l);
    echo $v->render(9);
}


public function modifiermdp($id,$mdp)
{
    $hash=password_hash($mdp,PASSWORD_DEFAULT,['cost'=> 12]);
    $u=User::all()->where("user_id","=","$id")->first();
    $u->password=$hash;
    $u->save();
}

}