<?php
/**
 * Created by PhpStorm.
 * User: svzsv
 * Date: 18/12/2017
 * Time: 11:25
 */

namespace mywishlist\vue;


class VuePrincipale
{
    public function render($select){
        $app = \Slim\Slim::getInstance();
        $res = <<<END
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>My Wishlist</title>

    <!-- Bootstrap core CSS -->
    <link href="{$app->urlFor('acceuil')}bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

 
    <!-- Custom styles for this template -->
    <link href="{$app->urlFor('acceuil')}starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="{$app->urlFor('acceuil')}bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{$app->urlFor('acceuil')}">My WishList</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="{$app->urlFor('acceuil')}">Accueil</a></li>
END;
        if(isset($_SESSION['user_id']))
            $res.="<li class=\"listes\"><a href=\"{$app->urlFor('listes')}\">Mes Listes</a></li>
                    <li class=\"listes\"><a href=\"{$app->urlFor('libre')}\">Listes publiques</a></li>
                   <li class=\"profil\"><a href='{$app->urlFor('profils',array('id'=>$_SESSION['user_id']))}'>Mon profil</a></li>";
        else{
            $res.="<li class=\"listes\"><a href=\"{$app->urlFor('libre')}\">Listes publiques</a></li>
                    <li class=\"login\"><a href=\"{$app->urlFor('connection')}\">Se connecter</a></li>
                     <li class=\"login\"><a href=\"{$app->urlFor('enregistrer')}\">S'enregistrer</a></li>";
    }
    $res.=<<<END
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      $select

    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="{$app->urlFor('acceuil')}/assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="{$app->urlFor('acceuil')}/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{$app->urlFor('acceuil')}/bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
END;
       
       return $res;
    }
}