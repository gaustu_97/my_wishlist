<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 18/12/2017
 * Time: 11:43
 */

namespace mywishlist\controleur;

use mywishlist\models\Item as Item;
use mywishlist\models\Liste;


class ControleurItem
{
    public function item($no,$id)
	{
            $l=Liste::all()->where('token','=',$no)->first();
            $c=Item::skip($id-1)->where('liste_id','=',$l['no'])->first();
            $v=new \mywishlist\vue\VueParticipant(array($c,$l));
            echo $v->render(3);
    }


    public function reserver($no,$item_id,$user,$mess)
    {
        $l=Liste::all()->where('token','=',$no)->first();
        $item=Item::skip($item_id-1)->where('liste_id','=',$l['no'])->first();
		$item->username = filter_var($user, FILTER_SANITIZE_SPECIAL_CHARS);
		$item->message=filter_var($mess, FILTER_SANITIZE_SPECIAL_CHARS);
        $item->save();
    }
	
	public function afficher_ajout_item($no)
{
    $v = new \mywishlist\vue\VueParticipant($l=Liste::all()->where('token','=',$no)->first());
    echo $v->render(6);
}

    public function afficher_modif_item($no,$id)
    {
        $l=Liste::all()->where('token','=',$no)->first();
        $c=Item::skip($id-1)->where('liste_id','=',$l['no'])->first();
        $v = new \mywishlist\vue\VueParticipant(array($l,$c));
        echo $v->render(7);
    }
    public function modif_item($nom, $description, $prix, $image,$url,$no, $id)
    {
            $l=Liste::all()->where('token','=',$no)->first();
            $item=Item::skip($id-1)->where('liste_id','=',$l['no'])->first();
            $item->nom=$nom;
            $item->descr=$description;
            $item->tarif=$prix;
            $item->img = $image;
            $item->url=$url;
            $item->save();
    }

    public function ajout_item($nom, $description, $prix, $image,$url, $liste)
	{
        if(isset($nom) && isset($description) && isset($prix) && isset($liste) && $liste!=0){
            $item = new Item();
            $liste=Liste::all()->where('token','=',$liste)->first()['no'];
            $item->liste_id=$liste;
            $item->nom=$nom;
            $item->descr=$description;
            $item->tarif=$prix;
            $item->img = $image;
            $item->url=$url;
            $item->save();
        }
    }

    public function supprimer_item($id)
    {
        $i = Item::where('id', '=', $id)->first();
        if (isset($_SESSION['user_id'])) {
            if ($i != null) {
                if ($_SESSION['user_id'] == Liste::all()->where('no','=',"$i->liste_id")->first()->user_id ){
                    $i->delete();
                } else {
                   return false;
                }
            } else {
                return false;
            }
        }
    }
}