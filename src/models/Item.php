<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 20/11/2017
 * Time: 11:31
 */

namespace mywishlist\models;


use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = "item";
    protected $primaryKey = "id";

    public $timestamps = false;

    public function liste(){
        return $this->belongsTo('mywishlist\models\Liste','liste_id');
    }
}