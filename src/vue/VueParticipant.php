<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 05/12/2017
 * Time: 11:23
 */

namespace mywishlist\vue;


class VueParticipant extends VuePrincipale
{
    private $tab;

    function __construct($tab)
    {
        $this->tab=$tab;
    }

    private function listesDeSouhaits(){
        $app = \Slim\Slim::getInstance();
        $res="<section><p>";
        if($_SERVER['REQUEST_URI']!="/libre")
        $res.="<form id='createListe' method='GET' action=\"{$app->urlFor('creerListe')}\">"."<button type='submit' name='ajout_liste' class='btn btn-primary'>ajouter une liste</button></form>";
        foreach ($this->tab as $row){
            $res .= "<hr/><a href={$app->urlFor('listes/items',array('no'=>$row['token']))}><h2>{$row['titre']}</h2></a>
                     <h3>{$row["description"]}</h3>";
            $res .= $row['expiration'].' : ' . $row['token'] . '<br/>';
        }
        return $res;
    }

    private function listesDeSouhaitsItems()
    {$app = \Slim\Slim::getInstance();
        $titre = $this->tab[0]["titre"];
        $res = "<h1>$titre</h1><p>{$this->tab[0]['description']}</p>";
        if (isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab[0]["user_id"]) {
                $s = explode("-", $this->tab[0]['expiration']);
                if (mktime(0, 0, 0, date('m'), date('d'), date('y')) < mktime(0, 0, 0, $s[1], $s[2], $s[0]))
                    $res .= "<form id='createItem' method='GET' action=\"{$app->urlFor('creerItem',array('no'=>$this->tab[0]["token"]))}\">" . "<button type='submit' name='ajout_item' class='btn btn-primary'>ajouter un item</button></form>";
                $res .= '<form id="formsupplist" method="POST" action="'.$app->urlFor('suppressionListe',array('no'=>$this->tab[0]["no"])) ."\">" . "<button type='submit' name='supprimer_liste' value='supprimer_liste' class='btn btn-primary'>Supprimer la liste</button></form>";
                $res .= '<form id="formmodlist" method="GET" action="' .$app->urlFor('modifliste',array('no'=>$this->tab[0]["token"])) . "\">" . "<button type='submit' name='modifier_liste' value='modifier_liste' class='btn btn-primary'>Modifier la liste</button></form>";
            }
        $i = 1;
        foreach ($this->tab[1] as $row) {
            $res .= '<hr/><tr>
                         <td><a href="'.$app->urlFor('item',array('no'=>$this->tab[0]["token"],'id'=>$i)).'" >'. $row['nom'] . '</a></td>
                         <td>' . $row['descr'] . '</td>
                     </tr><br/>';
            $i++;
        }
        $res .= '</table></section>';
        if(isset($_SESSION['user_id']))
            if($_SESSION['user_id']==$this->tab[0]["user_id"]) {
        $res.='<hr/>
        <label>Votre lien à partager :</label>
        <input type="text" class="form-control" readonly id="reservation_username" value="'. $_SERVER['HTTP_HOST']. $_SERVER['REQUEST_URI'] . '" name="reserve_username">';
    }
        return $res;
    }

    // Affichage précis d'un objet
    private function item(){
        $app = \Slim\Slim::getInstance();
        $res='<section><table><tr>';
        if($this->tab[0]["img"]!=NULL)
        $res.=' <img src='.$this->tab[0]["img"].' alt='.$this->tab[0]["img"].'></td>';
                    $res.= '<td>
						<div>
							<p><h1>'.$this->tab[0]['nom'].'</h1></p>
                            <table>
                                <tr>
                                    <td>'.$this->tab[0]['descr'].' </td>
                                    </tr>
                                    <td><strong>Prix : '.$this->tab[0]['tarif'].'€</strong></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr></table>';

        if($this->tab[0]["url"]!=NULL)
            $res.=' <a href='.$this->tab[0]["url"].'>'.$this->tab[0]["url"].'</a> </td><br/>';

                    $s=explode("-",$this->tab[1]['expiration']);
                    if(mktime(0,0,0,date('m'),date('d'),date('y'))<mktime(0,0,0, $s[1],$s[2],$s[0])) {
                        if (isset($_SESSION['user_id'])) {
                            if ($_SESSION['user_id'] == $this->tab[1]["user_id"]) {
                                if ($this->tab[0]["username"] == null) {
                                    $res .= '<form id=\'formsuppitem\' method=\'POST\' action="' . $app->urlFor('suppressionItem',array('id'=>$this->tab[0]['id'])) . '">'
                                        . '<input type="hidden" name="no" value="' . $this->tab[1]['token'] . '"> </input>'
                                        . '<button type=\'submit\' name=\'supprimer_item\' value=\'supprimer_item\' class=\'btn btn-primary\'>Supprimer</button></form>';
                                    $res .= '<form id=\'formmodifitem\' method=\'GET\' action="'. str_replace_first('items','modif',$_SERVER['REQUEST_URI']) . '">'
                                        . '<button type=\'submit\' name=\'modif_item\' value=\'modif_item\' class=\'btn btn-primary\'>Modifier</button></form>';
                                } else $res .= "<p>impossible de modifier cet item car il est deja réserver</p>";
                            }else if ($this->tab[0]["username"] == null) {
                                $res .= '<br/><br/><form method="post" id="reservation" class="form-inline" action=" ' . $_SERVER['REQUEST_URI'] . '">
                    <label for="reservation_username">Nom pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_username" name="reserve_username"><br/>
					<label>Message pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_message" name="reserve_message"><br/><br/>
                    <button type="submit" class="btn btn-primary" name=\'reserver\' value=\'reserver_f1\'>Réserver</button>
                   </form>';
                            } else $res .= "<br/><p>Item reservé par " . $this->tab[0]["username"]. "<br>".$this->tab[0]["message"]." </p>";
                        } else
                            if (isset($_COOKIE['user'])) {
                                if ($_COOKIE['user'] == $this->tab[1]["user_id"]) {
                                    $res .= "<p>Veuillez vous connecter pour avoir acces à vos items</p>";
                                } else if ($this->tab[0]["username"] == null) {
                                    $res .= '<br/><br/><form method="post" id="reservation" class="form-inline" action=" ' . $_SERVER['REQUEST_URI'] . '">
                    <label for="reservation_username">Nom pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_username" name="reserve_username"><br/>
					<label>Message pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_message" name="reserve_message"><br/><br/>
                    <button type="submit" class="btn btn-primary" name=\'reserver\' value=\'reserver_f1\'>Réserver</button>
                   </form>';
                                } else $res .= "<br/><p>Item reservé par " . $this->tab[0]["username"]. "<br>".$this->tab[0]["message"]." </p>";

                            } else if ($this->tab[0]["username"] == null) {
                                $res .= '<br/><br/><form method="post" id="reservation" class="form-inline" action=" ' . $_SERVER['REQUEST_URI'] . '">
                    <label for="reservation_username">Nom pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_username" name="reserve_username"><br/>
					<label>Message pour la réservation :</label><br/>
					<input type="text" class="form-control" id="reservation_message" name="reserve_message"><br/><br/>
                    <button type="submit" class="btn btn-primary" name=\'reserver\' value=\'reserver_f1\'>Réserver</button>
                   </form>';
                            } else $res .= "<br/><p>Item reservé par " . $this->tab[0]["username"]. "<br>".$this->tab[0]["message"]." </p>";
                    }
                    else if($this->tab[0]["username"]!=null) $res.= "<p>Item reservé par " . $this->tab[0]["username"]."<br/> Message : ".$this->tab[0]["message"]."</p>";
                     else $res.="<p>Item non reservé</p>";
        return $res.="</section>";
    }


    private function ajoutListe(){

        $app=\Slim\Slim::getInstance();
        $res="";
        if(isset($_SESSION['user_id']))
		$res.="<h1> Création d'une liste </h1>
		<section>
			<form id='form1' method='POST' action=\"{$app->urlFor("creationListe")}\">
				<div class='form-group'>
					<label class='col-form-label'> Titre </label>
					<input type='text' class='form-control' required name='titre'>
				</div>
				<div class='form-group'>
					<label class='col-form-label'> Description </label>
					<input type='text' class='form-control' name='descrip'>
				</div>
				<div class='form-group' >
						<label class='col-form-label'> Date d'expiration </label>
						<input type='date' class='form-control' required name='expi'>
				</div>
				<button type='submit' name='valider_liste' value='valid_liste_function' class='btn btn-primary'>Valider</button>
			</form>
		</section>
		";
        return $res;

    }

    private function modifListe(){
        $res="";
        if(isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab["user_id"]){
            $res="
		<h1> Modification d'une liste </h1>
		<section>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class='form-group'>
					<label class='col-form-label'> Titre </label>
					<input type='text' class='form-control' name='titre' required value='{$this->tab['titre']}'>
				</div>
				<div class='form-group'>
					<label class='col-form-label'> Description </label>
					<input type='text' class='form-control' name='descrip' value='{$this->tab['description']}'>
				</div>
				<div class='form-group' >
						<label class='col-form-label'> Date d'expiration </label>
						<input type='date' class='form-control' name='exp' required value='{$this->tab['expiration']}'>
				</div>
				<div class='form-group'>
					<label class='col-form-label'>Libre </label>
					<br/>";
        if($this->tab['libre']!="non")
						$res.="<input type=\"radio\"  name=\"libre\" value='oui' checked/><label>OUI</label>
						<input type=\"radio\" name=\"libre\" value='non'/><label>NON</label>";
        else $res.="<input type=\"radio\"  name=\"libre\" value='oui'/><label>OUI</label>
						<input type=\"radio\" name=\"libre\" value='non' checked/><label>NON</label>";
					
				$res.="</div>
				<button type='submit' name='valider_liste' value='valid_liste_function' class='btn btn-primary'>Valider</button>
			</form>
		</section>
		";}
        return $res;

    }

    private function ajoutItem(){
        $res="";
        if(isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab["user_id"])
        $res="<section>
		<h1> Création d'un nouvel item </h1>
		<section>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Nom </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"nom\" placeholder=\"Nom\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Description </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"descrip\" placeholder=\"Description\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Prix </label>
					<div class=\"col-xs-9\">
						<input type=\"number\" class=\"form-control\" name=\"prix\" min=\"0.00\" max=\"9999.99\" step=\"0.01\" value=\"0.00\" placeholder=\"Prix\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Image </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"image\" placeholder='Url'/>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Url </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"url\" placeholder='Url'/>
					</div>
				</div>
				
				
				<button type='submit' name='valider_item' value='valid_item_function' class='btn btn-default'>Valider</button> 
			</form>
		</section>";
        return $res;
    }

    private function modifItem(){
        $res="";
        if(isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab[0]["user_id"])
        $res="<section>
		<h1> modification de l'item </h1>
		<section>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Nom </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" value='{$this->tab[1]['nom']}' class=\"form-control\" name=\"nom\" placeholder=\"Nom\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Description </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" value='{$this->tab[1]['descr']}' class=\"form-control\" name=\"descrip\" placeholder=\"Description\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Prix </label>
					<div class=\"col-xs-9\">
						<input type=\"number\" value='{$this->tab[1]['tarif']}' class=\"form-control\" name=\"prix\" min=\"0.00\" max=\"9999.99\" step=\"0.01\" value=\"0.00\" placeholder=\"Prix\" required>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Image </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" value='{$this->tab[1]['img']}' class=\"form-control\" name=\"image\" placeholder='lien'/>
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Url </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" value='{$this->tab[1]['url']}' class=\"form-control\" name=\"url\" placeholder='Url'/>
					</div>
				</div>
				<button type='submit' name='valider_item' value='valid_item_function' class='btn btn-default'>Valider</button> 
			</form>
		</section>";
        return $res;
    }

    private function modifMdp(){
        $res="";
        if(isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab["user_id"])
                $res="<section>
		<h1> modification du mot de passe </h1>
		<section>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Mot de passe </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"mdp\" placeholder=\"mdp\" required>
					</div>
				</div>
				
				<button type='submit' name='valider_mdp' value='valid_mdp_function' class='btn btn-default'>Valider</button> 
			</form>
		</section>";
        return $res;
    }

    public function Profil(){
        $app = \Slim\Slim::getInstance();
        $res="<h1>{$this->tab['username']}</h1>";
        if(isset($_SESSION['user_id']))
            if ($_SESSION['user_id'] == $this->tab["user_id"]) {
                $res .= '<form id=\'formmodifmdp\' method=\'GET\' action="'. $app->urlFor('modifmdp',array('id'=>$this->tab["user_id"])) . '"/>'
                    . '<button type=\'submit\' name=\'modif_item\' value=\'modif_item\' class=\'btn btn-primary\'>Modifier</button></form>';

                $res .= '<form id=\'formsuppuser\' method=\'POST\' action="' . $_SERVER["REQUEST_URI"] . '"/>'
                    . '<button type=\'submit\' name=\'supp_user\' value=\'supp_user\' class=\'btn btn-primary\'>Supprimer</button></form>';
            }
        return $res;
    }

    function render($select){
        $res="";
        switch ($select){
            case 1:
                $res.= $this->listesDeSouhaits();
                break;
            case 2:
                $res.= $this->listesDeSouhaitsItems();
                break;
            case 3:
                $res = $this->item();
                break;
            case 5:
                $res = $this->ajoutListe();
                break;
            case 6:
                $res = $this->ajoutItem();
                break;
            case 7:
                $res = $this->modifItem();
                break;
            case 8:
                $res = $this->modifListe();
                break;
            case 9:
                $res = $this->modifMdp();
                break;
            case 10:
                $res = $this->Profil();
                break;
        }
		return parent::render($res);
	}
}