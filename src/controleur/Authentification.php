<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 18/12/2017
 * Time: 10:54
 */

namespace mywishlist\controleur;

use mywishlist\models\User;
use Slim\Slim;

class Authentification
{
    public static function createUser($username,$pwd)
    {

        if(true==true){
            $hash=password_hash($pwd,PASSWORD_DEFAULT,['cost'=> 12]);
            $user = new user();
            $user->user_id=NULL;
            $user->username=$username;
            $user->password=$hash;
            $user->save();
        }
    }

    public static function authentificate($user,$pass){

            $u = User::where("username", "=", $user)->first();
            if(!$u==null) {
                if (password_verify($pass, $u->password)) {
                    self::loaduser($user);
                } else {
                    return false;
                }
            }else{
                return false;
            }
            return true;
    }

    public static function loaduser($username)
    {
        if (isset($_SESSION)) {
            session_destroy();
        }
        session_start();
        $u = User::where("username", "=", $username)->first();
        $_SESSION['level'] = $u->level;
        $_SESSION['name'] = $u->username;
        $_SESSION['user_id'] = $u->user_id;
        if(isset($_COOKIE['user']))
            \Slim\Slim::getInstance()->setcookie('user',"");
        \Slim\Slim::getInstance()->setcookie('user',$u->user_id,time()+5e+6);
    }

    public static function checkPrivilege($value){
        if($value>$_SESSION['level'])return false;
        return true;
    }

    public function changeName($name){
        $u = User::where("username", "=", $_SESSION['name'])->first();
        $u->username=$name;
        $u->save();
        session_destroy();
    }

    public function showProfileInformations() {
        $view = new \mywishlist\vue\VueConnection();
        echo $view->render(3);
    }

    public static function changePassword($oldpass,$pass){

        $u = User::where("username", "=", $_SESSION['name'])->first();
        if(!$u==null) {
            if (password_verify($oldpass, $u->password)) {
                $u->password=password_hash($pass,PASSWORD_DEFAULT,['cost'=> 12]);
            } else {
                return false;
            }
        }else{
            return false;
        }
        return true;
    }
}