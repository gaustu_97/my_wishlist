<?php
/**
 * Created by PhpStorm.
 * User: gstuh
 * Date: 19/12/2017
 * Time: 09:58
 */

namespace mywishlist\vue;


class VueConnection extends VuePrincipale
{
    private function creation(){
        $res="
		<h3>Authentification</h3>
		<section>
			<h4>Inscription<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Identifiant </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"name\">
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Mot de passe </label>
					<div class=\"col-xs-9\">
						<input type=\"password\" class=\"form-control\" name=\"pwd\">
					</div>
				</div>
				<div class=\"form-group row\">
					<div class=\"offset-xs-3 col-xs-9\">
						<button type=\"submit\" name='valider_inc' value='valid_f1' class=\"btn btn-default\">S'inscrire</button>
					</div>
				</div>
			</form>
		</section>
		";
        return $res;
    }

    private function connection(){

		$res = "
		<h3>Authentification</h3>
		<section>
			<h4>Connexion<br></h4>
			<form id='form1' method='POST' action=\"{$_SERVER['REQUEST_URI']}\">
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Identifiant </label>
					<div class=\"col-xs-9\">
						<input type=\"text\" class=\"form-control\" name=\"name\">
					</div>
				</div>
				<div class=\"form-group row\">
					<label class=\"col-xs-3 col-form-label mr-2\"> Mot de passe </label>
					<div class=\"col-xs-9\">
						<input type=\"password\" class=\"form-control\" name=\"pwd\">
					</div>
				</div>
				<div class=\"form-group row\">
					<div class=\"offset-xs-3 col-xs-9\">
						<button type=\"submit\" name='valider_inc' value='valid_f1' class=\"btn btn-default\">Se connecter</button>
					</div>
				</div>
			</form>
		</section>
		";
        return $res;
    }


    function render($select){
        $res="";
        switch ($select){
            case 1:
                $res.= $this->connection();
                break;
            case 2:
                $res.= $this->creation();
                break;
        }
        return parent::render($res);
    }

}