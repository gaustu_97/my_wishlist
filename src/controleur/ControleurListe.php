<?php
/**
 * Created by PhpStorm.
 * User: kylia
 * Date: 18/12/2017
 * Time: 11:44
 */

namespace mywishlist\controleur;

use Illuminate\Support\Facades\Session;
use mywishlist\models\Item as Item ;
use mywishlist\models\Liste as Liste;
use mywishlist\models\User;


class ControleurListe
{

    public function liste(){
        if(isset($_SESSION['user_id']))
            $id=$_SESSION['user_id'];
        else $id=0;
        $c=Liste::all()->where('user_id','=',"$id")->toArray();
        $v=new \mywishlist\vue\VueParticipant($c);
        echo $v->render(1);
    }

    public function libre(){
        $c=Liste::all()->where('libre','=',"oui")->toArray();
        $v=new \mywishlist\vue\VueParticipant($c);
        echo $v->render(1);
    }

    function liste_item($num){
        $liste=Liste::all()->where('token','=',$num)->first();

        if(!isset($liste))echo('La liste demandée n\'existe pas.');
        else {
            $c=array($liste, Liste::all()->where('token','=',$num)->first()->items()->get()->toArray());
            $v=new \mywishlist\vue\VueParticipant($c);
            echo $v->render(2);
        }
    }

    public function ajout_liste($titre,$description ,$expiration){
		$user_id = $_SESSION['user_id'];

		if(isset($titre) && isset($description) && isset($expiration)){
			$liste = new Liste();
			$liste->no=NULL;
			$liste->user_id=$user_id;
			$liste->titre=$titre;
			$liste->description=$description;
			$liste->expiration=$expiration;
            $liste->libre="non";
			$token=rand(1,200000);

			while(count(Liste::all()->where('token','=',$token)->toArray())>0)
                $token=rand(1,200000);

			$liste->token=$token;
			$liste->save();
		}
    }

    public function afficher_nListe(){
        $v = new \mywishlist\vue\VueParticipant(null);
        echo $v->render(5);
    }

    public function supprimer_liste($id)
    {
        $l = Liste::where('no', '=', $id)->first();
        if (isset($_SESSION['user_id'])) {
            if ($l != null) {
                if ($_SESSION['user_id']  == $l->user_id) {
                    $l->items()->delete();
                    $l->delete();
                } else {
                   return false;
                }
            } else {
                return false;
            }
        }
    }

    public function afficher_modif_liste($token){
        $l=Liste::all()->where('token','=',$token)->first();
        $v = new \mywishlist\vue\VueParticipant($l);
        echo $v->render(8);
    }

    public function modif_liste($titre, $description, $exp,$libre,$no)
    {
        $l=Liste::all()->where('token','=',$no)->first();
        $l->titre=$titre;
        $l->description=$description;
        $l->expiration=$exp;

        $l->libre=$libre;
        $l->save();
    }

}